package crudoperation;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class UpdateStudentDetails {

	public UpdateStudentDetails () {
		
		JFrame frame=new JFrame("UpdateStudentDetails");
		
		 frame.setSize(400,500);
		 
		 JLabel ID=new JLabel("Student_ID");
		 ID.setBounds(23,45,100,30);
		 frame.add(ID);
		  
		 JTextField IDField=new JTextField();
		 IDField.setBounds(200,45,150,30);
		 frame.add(IDField);
		 
		 JLabel Student_name=new JLabel("Student_Name");
		 Student_name.setBounds(23,100,150,30);
		 frame.add(Student_name);
		 
		 JTextField Student_nameField=new JTextField();
		 Student_nameField.setBounds(200,100,150,30);
		 frame.add(Student_nameField);
		 
		 JLabel Student_PhoneNumber=new JLabel("Student_PhoneNumber");
		 Student_PhoneNumber.setBounds(23,150,150,30);
		 frame.add(Student_PhoneNumber);
		 
		 JTextField Student_PhoneNumber1=new JTextField();
		 Student_PhoneNumber1.setBounds(200,150,150,30);
		 frame.add(Student_PhoneNumber1);
		  
		 JLabel city_name=new JLabel("Student_City ");
		 city_name.setBounds(23,200,100,30);
		 frame.add(city_name);
		 
		 JTextField city_nameField=new JTextField();
		 city_nameField.setBounds(200,200,150,30);
		 frame.add(city_nameField);
		  
		 JButton Update=new JButton();
		 Update.setText("Update");
		 Update.setBounds(150,300,100,30);
	 	 Update.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){  
				 try{  
					 Class.forName("oracle.jdbc.driver.OracleDriver");  
					 Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","123456");  
					 String studentname=Student_nameField.getText();
					 int Id=Integer.parseInt(IDField.getText());
					 Long studentphonenumber=Long.parseLong(Student_PhoneNumber.getText());
					 String studentcity=city_nameField.getText();
					 PreparedStatement stmt=con.prepareStatement("update studentdetails1 set Student_Name='"+studentname+"',Student_City='"+studentcity+",Student_PhoneNumber='"+studentphonenumber+"' where Student_ID="+Id);
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+"record updated"); 
					 System.out.println(i+" record updated");    
					 con.close();  
					 }catch(Exception e1){ 
						 System.out.println(e1);}  
					   
			 		}
		             
		        }  );
	 frame.add(Update);
	 frame.setLayout(null);
	 frame.setVisible(true);
	}

	}

