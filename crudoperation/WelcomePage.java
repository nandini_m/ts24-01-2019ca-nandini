package crudoperation;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.Statement;

public class WelcomePage {
	
	public static void main(String[] args) {
		JFrame f=new JFrame("Welcome Page");
		
		JButton btn1,btn2,btn3;
		btn1 = new JButton("Insert");
		btn1.setBounds(100,100,95,30);
	   

		btn2 = new JButton("Update");
		btn2.setBounds(100,150,95,30);
		 
		btn3 = new JButton("Delete");
		btn3.setBounds(100,200,95,30);
		 f.add(btn1);
		 f.add(btn2);
		 f.add(btn3);
		 
		 btn1.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new InsertStudentDetails();
				 f.dispose();
			 }});
		 
		 btn2.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new UpdateStudentDetails();
				 f.dispose();
			 }});

		 
		 btn3.addActionListener(new ActionListener(){  
			 public void actionPerformed(ActionEvent e){ 
				 new DeleteStudentDetails();
				 f.dispose();
			 }});
		
		
		 f.setSize(300, 300);
		 f.setLayout(null);
		 f.setVisible(true);



	
}

	}
